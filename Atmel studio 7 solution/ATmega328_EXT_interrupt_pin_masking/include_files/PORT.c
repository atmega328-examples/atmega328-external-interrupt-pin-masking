/*
 * PORT.c
 *
 * Created: 05-03-2019 15:56:48
 *  Author: NISI
 */ 

#include <avr/io.h> // avr macros
#include "PORT.h"
#include <string.h>

//initializes a ports bit
void initPortDir(char *DataDirReg, char BitNo) {
	
	if (strcmp(DataDirReg, "DDRB") == 0) {
		DDRB |= BitNo;
	}
	if (strcmp(DataDirReg, "DDRC") == 0) {
		DDRC |= BitNo;
	}
	if (strcmp(DataDirReg, "DDRD") == 0) {
		DDRD |= BitNo;
	}
	else {
		//do nothing
	}
}

void clearBit(char *DataReg, char BitNo) {
	
	if (strcmp(DataReg, "PORTB") == 0) {
		PORTB &=~ BitNo; //turn off led bit, leave rest with AND mask
	}
	if (strcmp(DataReg, "PORTC") == 0) {
		PORTC &=~ BitNo; //turn off led bit, leave rest with AND mask
	}
	if (strcmp(DataReg, "PORTD") == 0) {
		PORTD &=~ BitNo; //turn off led bit, leave rest with AND mask
	}
	else {
		//do nothing
	}
}

void setBit(char *DataReg, char BitNo) {
	
	if (strcmp(DataReg, "PORTB") == 0) {
		PORTB |= BitNo; //turn off led bit, leave rest with AND mask
	}
	if (strcmp(DataReg, "PORTC") == 0) {
		PORTC |= BitNo; //turn off led bit, leave rest with AND mask
	}
	if (strcmp(DataReg, "PORTD") == 0) {
		PORTD |= BitNo; //turn off led bit, leave rest with AND mask
	}
	else {
		//do nothing
	}
}

void toggleBit(char *DataReg, char BitNo) {
	
	if (strcmp(DataReg, "PORTB") == 0) {
		PORTB ^= BitNo; //turn off led bit, leave rest with AND mask
	}
	if (strcmp(DataReg, "PORTC") == 0) {
		PORTC ^= BitNo; //turn off led bit, leave rest with AND mask
	}
	if (strcmp(DataReg, "PORTD") == 0) {
		PORTD ^= BitNo; //turn off led bit, leave rest with AND mask
	}
	else {
		//do nothing
	}
}