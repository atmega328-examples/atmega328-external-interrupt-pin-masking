/*
 * ATmega328_EXT_interrupt_pin_masking.c
 *
 * inspiration from: https://sites.google.com/site/qeewiki/books/avr-guide/external-interrupts-on-the-atmega328
 *
 * Created: 25-03-2019 15:42:30
 * Author : nisi
 */ 

#define F_CPU 16000000UL //use 16MHz
#include <avr/io.h>
#include <stdio.h>
#include <avr/interrupt.h>    // Needed to use interrupts
#include "include_files/STDIO_UART.h"
#include "include_files/PORT.h"

#define BAUD 9600 //BAUD Rate for UART
volatile char portbhistory = 0xFF;     // default is high because of pull-up
const char led0 = 0b00000100; //PD2
const char led1 = 0b00001000; //PD3
const char button1 = 0b00000100; //PB2
const char button2 = 0b00001000; //PB3

int main(void)
{
	//ioinit(); //Setup uart and STDIO. Use printf etc. if you need to debug!
	initPortDir("DDRD", led0); //init led0 as output
	initPortDir("DDRD", led1); //init led1 as output
	
	setBit("PORTB", button1); //enable button1 pullup 
	setBit("PORTB", button2); //enable button2 pullup

	// Setup change interrupt
	PCICR |= (1 << PCIE0);     // set PCIE0 to enable PCMSK0 scan
	PCMSK0 |= (1 << PCINT2) | (1 << PCINT3);   // set PCINT2+3 to trigger an interrupt on state change, add more if needed
	
	sei(); //enable interrupts

	while (1)
	{
		//everything happens in external interrupt
	}
}

//PortB change interrupt routine with bit masking
ISR (PCINT0_vect) {
	
	char changedbits;
	changedbits = PINB ^ portbhistory;
	portbhistory = PINB;
	
	if (changedbits & button1) { // button1 changed? 
		if( (PINB & button1) ) { // button1 high?
			/* LOW to HIGH pin change */
			//toggleLed(led1);
		}
		else { //button1 must be low!
			/* HIGH to LOW pin change */
			toggleBit("PORTD", led1); //led1 will light on button1 release
		}
	}
	
	if (changedbits & button2) {
		if( (PINB & button2) ) {
			/* LOW to HIGH pin change */
			toggleBit("PORTD", led0);
		}
		else {
			/* HIGH to LOW pin change */
			//toggleLed(led0);
		}
	}	
		
	//add more if needed
}